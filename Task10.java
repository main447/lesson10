import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner myscan = new Scanner (System.in);
        System.out.println("Введите дату в формате dd.MM.yyyy");
        String date = myscan.nextLine();
        String year = date.substring(date.length() - 4);
        int y = Integer.parseInt(year);
        if(y%4==0){
            System.out.println("год високосный");
        }
        else{
            System.out.println("год не високосный");
        }
    }
}
